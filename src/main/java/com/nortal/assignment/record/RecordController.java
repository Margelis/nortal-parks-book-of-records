package com.nortal.assignment.record;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nortal.assignment.common.CustomDateSerializer;

/**
 * Controller that manages Record entries.
 */
@Controller
@RequestMapping(value = "/record")
public class RecordController {

	private static final Logger LOG = LoggerFactory.getLogger(RecordController.class);

	@Resource
	private RecordService recordService;

	@RequestMapping(method = RequestMethod.GET)
	public String getView() {
		return "record";
	}

	@RequestMapping(value = "export", method = RequestMethod.GET)
	public HttpEntity<byte[]> getDataExport(Model model) throws IOException {

		byte[] documentBody;
		StringBuilder csv = new StringBuilder();
		
		csv.append("Id;Date;Type;Employees;Projects;Record content\r\n");

		for (Record record : recordService.getListData()) {
			csv.append(record.getId() + ";"
					+ CustomDateSerializer.FORMATTER.format(record.getDate()) + ";"
					+ record.getTypeString() + ";" 
					+ "\"" + record.getEmployeeNames() + "\";"
					+ "\"" + record.getProjectNames() + "\";"
					+ "\"" + record.getContent() + "\"\r\n");

		}

		documentBody = csv.toString().getBytes();

		HttpHeaders header = new HttpHeaders();
		header.setContentType(new MediaType("text", "csv"));
		header.set("Content-Disposition", "attachment; filename=report.csv");
		header.setContentLength(documentBody.length);
		
		LOG.info("CSV file generated.");
		return new HttpEntity<byte[]>(documentBody, header);
	}

	@RequestMapping(value = "data", produces = { "application/json" }, method = RequestMethod.GET)
	@ResponseBody
	public List<Record> getData(Model model) {
		LOG.info("Getting data list.");
		return recordService.getListData();
	}

	@RequestMapping(value = "save", method = RequestMethod.POST)
	public void save(@RequestBody final Record record) {
		LOG.info("Saving record: {}", record.toString());
		recordService.save(record);
	}

	@RequestMapping(value = "delete", method = RequestMethod.POST)
	public void delete(@RequestBody final Record record) {
		LOG.info("Deleting record: {}", record.toString());
		recordService.delete(record.getId());
	}

}
