package com.nortal.assignment.record;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.nortal.assignment.common.CustomDateSerializer;
import com.nortal.assignment.employee.Employee;
import com.nortal.assignment.project.Project;

/**
 * 
 * @author Priit Liivak
 * 
 */
public class Record {

	private Long id;
	private List<Employee> employees;
	private List<Project> projects;
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date date;
	private RecordType type;
	private String content;

	/**
	 * @return Single String of comma separated employee names ordered by salary
	 */
	public String getEmployeeNames() {
		String emplNames = "";

		sortEmployeesBySalaryDesc();
		emplNames = getEmployeeNamesInCommaSep();

		return emplNames;
	}

	/**
	 * @return Single String of comma separated project names ordered
	 *         alphabetically
	 */
	public String getProjectNames() {
		String projects = "";

		sortProjectNamesAlphabetically();
		projects = getProjectsInCommaSep();

		return projects;
	}

	private void sortProjectNamesAlphabetically() {
		Collections.sort(projects, new Comparator<Project>() {

			@Override
			public int compare(Project p1, Project p2) {
				return p1.getProjectName().compareTo(p2.getProjectName());
			}
		});
	}

	private String getEmployeeNamesInCommaSep() {
		StringBuilder commaSepValueBuilder = new StringBuilder();

		for (int i = 0; i < employees.size(); i++) {
			commaSepValueBuilder.append(employees.get(i).getEmployeeName());

			if (i != employees.size() - 1) {
				commaSepValueBuilder.append(", ");
			}
		}

		return commaSepValueBuilder.toString();
	}

	private String getProjectsInCommaSep() {
		StringBuilder commaSepValueBuilder = new StringBuilder();

		for (int i = 0; i < projects.size(); i++) {
			commaSepValueBuilder.append(projects.get(i).getProjectName());

			if (i != projects.size() - 1) {
				commaSepValueBuilder.append(", ");
			}
		}

		return commaSepValueBuilder.toString();
	}

	private void sortEmployeesBySalaryDesc() {
		Collections.sort(employees, new Comparator<Employee>() {

			@Override
			public int compare(Employee e1, Employee e2) {
				return e2.getSalary().compareTo(e1.getSalary());
			}
		});
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public RecordType getType() {
		return type;
	}

	public void setType(RecordType type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTypeString() {
		return type == null ? null : type.name();
	}

	public void setTypeString(String typeString) {
		if (StringUtils.isBlank(typeString))
			type = null;
		else
			type = RecordType.valueOf(typeString);
	}

	@Override
	public String toString() {
		return "Record [id=" + id + ", employees=" + employees + ", projects=" + projects + ", date=" + date + ", type="
				+ type + ", content=" + content + "]";
	}

}
