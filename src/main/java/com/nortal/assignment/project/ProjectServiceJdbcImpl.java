package com.nortal.assignment.project;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Service;

/**
 * @author Priit Liivak
 *
 */
@Service
public class ProjectServiceJdbcImpl implements ProjectService {
	private static final Logger LOG = LoggerFactory.getLogger(ProjectServiceJdbcImpl.class);

	private final class projectMapper implements RowMapper<Project> {
		@Override
		public Project mapRow(ResultSet rs, int rowNum) throws SQLException {
			Project project = new Project();
			project.setId(rs.getLong("id"));
			project.setProjectName(rs.getString("project_name"));

			return project;
		}
	}

	@Resource
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Project> getListData() {
		String sql = "select id, project_name from project";
		List<Project> projects = jdbcTemplate.query(sql, new projectMapper());
		LOG.info("Projects found: {}", projects.toString());

		return projects;
	}

	@Override
	public void save(Project project) {
		String sql;
		List<Object> args = new ArrayList<>();
		args.add(project.getProjectName());

		if (project.getId() == null) {
			sql = "insert into project (project_name) values (?)";
			LOG.info("Project saved: {}", project.toString());
		} else {
			sql = "update project set project_name=? where id= ?";
			args.add(project.getId());
			LOG.info("Project updated: {}", project.toString());
		}

		jdbcTemplate.update(sql, args.toArray());
	}

	@Override
	public void delete(Long projectId) {
		String sqlRecord = "delete from record_project where project_id=?";
		String sqlTimetable = "delete from timetable where project_id=?";
		String sqlProject = "delete from project where id=?";

		jdbcTemplate.update(sqlRecord, projectId);
		jdbcTemplate.update(sqlTimetable, projectId);
		jdbcTemplate.update(sqlProject, projectId);
		LOG.info("Project with ID {} deleted.", projectId);
	}

	@Override
	public Project getProjectWithMostRecords() {
		// @formatter:off
		String sql = "SELECT p.* FROM project p where p.id in " + "(SELECT project_id FROM "
				+ "(SELECT rp.project_id, COUNT(rp.record_id) record_count FROM record_project rp GROUP BY rp.project_id) proj_rec_count "
				+ "ORDER BY record_count DESC  LIMIT 1) ";
		// @formatter:on
		Project project = jdbcTemplate.queryForObject(sql, new projectMapper());
		LOG.info("Project with most records found: {}", project.toString());
		return project;
	}

	@Override
	public Project getBestExecutedProject() {
		Project project = getExecutedProjectGrading(false);
		LOG.info("Best executed project found: {}", project.toString());
		return project;
	}

	@Override
	public Project getWorstExecutedProject() {
		Project project = getExecutedProjectGrading(true);
		LOG.info("Worst executed project found: {}", project.toString());
		return project;
	}

	private Project getExecutedProjectGrading(boolean positiveMinusNegative) {
		// @formatter:off
		String sql = "select * from project p where p.id in (select project_id from( "
				+ "select project_id, sum(record_count) total_count from ( " + "select negative_records.project_id, "
				+ (positiveMinusNegative ? "" : "-") + "count(negative_records.record_id) record_count from( "
				+ "select rp.* from record_project rp join record r on r.id=rp.record_id where r.type_code in ('NEGATIVE', 'VERY_NEGATIVE') "
				+ ") as negative_records group by negative_records.project_id " + "union "
				+ "select positive_records.project_id, " + (positiveMinusNegative ? "-" : "")
				+ "count(positive_records.record_id) record_count from( "
				+ "select rp.* from record_project rp join record r on r.id=rp.record_id where r.type_code in ('POSITIVE', 'VERY_POSITIVE') "
				+ ") as positive_records group by positive_records.project_id "
				+ ") group by project_id order by total_count LIMIT 1 " + ")) ";
		// @formatter:on
		return jdbcTemplate.queryForObject(sql, new projectMapper());
	}

	@Override
	public Project findProjectById(Long id) {
		String sql = "select id, project_name from project where id=?";
		Project project = jdbcTemplate.queryForObject(sql,
				ParameterizedBeanPropertyRowMapper.newInstance(Project.class), id);
		LOG.info("Project found by ID " + id + ". {}", project.toString());
		return project;
	}

}
