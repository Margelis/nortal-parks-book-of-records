package com.nortal.assignment.employee;

import java.math.BigDecimal;
import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.nortal.assignment.common.CustomDateSerializer;

/**
 *
 * @author Priit Liivak
 *
 */
public class Employee {

  private Long id;
  private String employeeName;
  private String kind;
  private BigDecimal salary;
  @JsonSerialize(using = CustomDateSerializer.class)
  private Date startDate;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getEmployeeName() {
    return employeeName;
  }

  public void setEmployeeName(String employeeName) {
    this.employeeName = employeeName;
  }

  public String getKind() {
    return kind;
  }

  public void setKind(String kind) {
    this.kind = kind;
  }

  public BigDecimal getSalary() {
    return salary;
  }

  public void setSalary(BigDecimal salary) {
    this.salary = salary;
  }

@Override
public String toString() {
	return "Employee [id=" + id + ", employeeName=" + employeeName + ", kind=" + kind + ", salary=" + salary
			+ ", startDate=" + startDate + "]";
}

public Date getStartDate() {
	return startDate;
}

public void setStartDate(Date startDate) {
	this.startDate = startDate;
}


}
