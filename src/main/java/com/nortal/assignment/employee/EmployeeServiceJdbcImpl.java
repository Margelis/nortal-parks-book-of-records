package com.nortal.assignment.employee;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Service;

/**
 * @author Priit Liivak
 * 
 */
@Service
public class EmployeeServiceJdbcImpl implements EmployeeService {
	private static final Logger LOG = LoggerFactory.getLogger(EmployeeServiceJdbcImpl.class);
	@Resource
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Employee> getListData() {
		String sql = "select id, employee_name, kind, salary, start_date from employee";
		List<Employee> emp = jdbcTemplate.query(sql, new EmployeeRowMapper());
		LOG.info("Employees found: {}", emp.toString());
		return emp;
	}

	@Override
	public void save(Employee employee) {
		String sql;
		List<Object> args = new ArrayList<>();
		args.add(employee.getEmployeeName());
		args.add(employee.getKind());
		args.add(employee.getSalary());
		args.add(employee.getStartDate());

		if (employee.getId() == null) {
			sql = "insert into employee (employee_name, kind, salary, start_date) values (?,?,?,?)";
			LOG.info("Employee saved: {}", employee.toString());
		} else {
			sql = "update employee set employee_name=?, kind=?, salary=?, start_date=? where id= ?";
			args.add(employee.getId());
			LOG.info("Employee updated: {}", employee.toString());
		}

		jdbcTemplate.update(sql, args.toArray());
	}

	@Override
	public void delete(Long employeeId) {
		String sqlRecord = "delete from record_employee where employee_id=?";
		String sqlTimetable = "delete from timetable where employee_id=?";
		String sqlEmpl = "delete from employee where id=?";

		jdbcTemplate.update(sqlRecord, employeeId);
		jdbcTemplate.update(sqlTimetable, employeeId);
		jdbcTemplate.update(sqlEmpl, employeeId);
		LOG.info("Empoyee with ID {} deleted.", employeeId);
	}

	public final class EmployeeRowMapper implements RowMapper<Employee> {

		@Override
		public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
			Employee employee = new Employee();
			employee.setId(rs.getLong("id"));
			employee.setEmployeeName(rs.getString("employee_name"));
			employee.setKind(rs.getString("kind"));
			employee.setSalary(rs.getBigDecimal("salary"));
			employee.setStartDate(rs.getDate("start_date"));

			return employee;
		}
	}

	@Override
	public Employee findEmployeeById(Long id) {
		String sql = "select id, employee_name, kind, start_date, salary from employee where id=?";
		Employee employee = jdbcTemplate.queryForObject(sql,
				ParameterizedBeanPropertyRowMapper.newInstance(Employee.class), id);
		LOG.info("Employee found by ID " + id + ". {}", employee.toString());
		return employee;
	}

}
