package com.nortal.assignment.timetable;

import com.nortal.assignment.employee.EmployeeService;
import com.nortal.assignment.project.ProjectService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Priit Liivak
 */
@Service
public class TimetableServiceJdbcImpl implements TimetableService {
	private static final Logger LOG = LoggerFactory.getLogger(TimetableServiceJdbcImpl.class);
  @Resource
  private JdbcTemplate jdbcTemplate;
  
  @Autowired
  private EmployeeService emplService;
  
  @Autowired
  private ProjectService projectService;

  @Override
  public List<Timetable> getListData() {
	  String sql = "select id, employee_id, project_id, start_date, end_date from timetable";
	  
	  List<Timetable> timetables = jdbcTemplate.query(sql, new TimetableRowMapper());
	  LOG.info("Timetables list data found: {}", timetables.toString());
	  return timetables;
  }

  @Override
  public void save(final Timetable data) {
	  String sql;
	    List<Object> args = new ArrayList<>();
	    args.add(data.getEmployee().getId());
	    args.add(data.getProject().getId());
	    args.add(data.getStartDate());
	    args.add(data.getEndDate());

	    if (data.getId() == null) {
	      sql = "insert into timetable (employee_id, project_id, start_date, end_date) values (?,?,?,?)";
	      LOG.info("Timetable saved: {}", data.toString());
	    } else {
	      sql = "update timetable set employee_id=?, project_id=?, start_date=?, end_date=? where id= ?";
	      args.add(data.getId());
	      LOG.info("Timetable updated: {}", data.toString());
	    }

	    jdbcTemplate.update(sql, args.toArray());
  }

  @Override
  public void delete(Long id) {
    String recordSql = "DELETE FROM timetable WHERE id=?";
    jdbcTemplate.update(recordSql, id);
	LOG.info("Timetable with ID {} deleted", id);
  }

	public final class TimetableRowMapper implements RowMapper<Timetable> {

		@Override
		public Timetable mapRow(ResultSet rs, int rowNum) throws SQLException {
			Timetable timetable = new Timetable();
			timetable.setId(rs.getLong("id"));
			timetable.setEmployee(emplService.findEmployeeById(rs.getLong("employee_id")));
			timetable.setProject(projectService.findProjectById(rs.getLong("project_id")));
			timetable.setStartDate(rs.getDate("start_date"));
			timetable.setEndDate(rs.getDate("end_date"));

			return timetable;
		}
	}
}
