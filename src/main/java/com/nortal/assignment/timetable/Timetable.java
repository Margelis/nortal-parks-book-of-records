package com.nortal.assignment.timetable;

import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.nortal.assignment.common.CustomDateTimeDeserializer;
import com.nortal.assignment.common.CustomDateTimeSerializer;
import com.nortal.assignment.employee.Employee;
import com.nortal.assignment.project.Project;

public class Timetable {

  private Long id;
  private Employee employee;
  private Project project;
  @JsonSerialize(using = CustomDateTimeSerializer.class)
  @JsonDeserialize(using = CustomDateTimeDeserializer.class)
  private Date startDate;
  @JsonSerialize(using = CustomDateTimeSerializer.class)
  @JsonDeserialize(using = CustomDateTimeDeserializer.class)
  private Date endDate;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Employee getEmployee() {
    return employee;
  }

  public void setEmployee(Employee employee) {
    this.employee = employee;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public String getWorkedTime() {
    return String.valueOf(hoursDifference(endDate, startDate));
  }

  private static int hoursDifference(Date date1, Date date2) {

	    final int MILLI_TO_HOUR = 1000 * 60 * 60;
	    return (int) (date1.getTime() - date2.getTime()) / MILLI_TO_HOUR;
	}

	@Override
	public String toString() {
		return "Timetable [id=" + id + ", employee=" + employee + ", project=" + project + ", startDate=" + startDate
				+ ", endDate=" + endDate + "]";
	}

}
