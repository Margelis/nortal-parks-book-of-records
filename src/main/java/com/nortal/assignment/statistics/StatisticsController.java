package com.nortal.assignment.statistics;

import javax.annotation.Resource;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller that manages Statistics.
 */
@Controller
@RequestMapping(value = "/statistics")
public class StatisticsController {

	private static final Logger LOG = LoggerFactory.getLogger(StatisticsController.class);

	@Resource
	private StatisticsService statisticsService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView getView() throws InterruptedException {
		final ModelAndView modelAndView = new ModelAndView("statistics");

		Thread thread1 = new Thread(new Runnable() {
			@Override
			public void run() {
				modelAndView.addObject("bestExecutedProject", statisticsService.getBestExecutedProject());
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		
		Thread thread2 = new Thread(new Runnable() {
			@Override
			public void run() {
				modelAndView.addObject("projectWithMostRecords", statisticsService.getProjectWithMostRecords());
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		
		Thread thread3 = new Thread(new Runnable() {
			@Override
			public void run() {
				modelAndView.addObject("worstExecutedProject", statisticsService.getWorstExecutedProject());
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		
		thread1.start();
		thread2.start();
		thread3.start();
		
		thread1.join();
		thread2.join();
		thread3.join();
		
		return modelAndView;
	}

	@RequestMapping(value = "employeeInvolvement", produces = { "application/json" }, method = RequestMethod.GET)
	@ResponseBody
	public Statistics getEmployeeInvolvement() {
		LOG.info("Getting Employee count group by project.");
		return statisticsService.getEmployeeCountGroupedByProject();
	}

	@RequestMapping(value = "satisfactionRecords/{projectId}", produces = {
			"application/json" }, method = RequestMethod.GET)
	@ResponseBody
	public Statistics getSatisfactionRecordsData(@PathVariable final int projectId) {
		LOG.info("Getting satisfaction records by project.");
		return statisticsService.getSatisfactionRecordsByProject(projectId);
	}

	@RequestMapping(value = "employeeRecords/{projectId}", produces = {
			"application/json" }, method = RequestMethod.GET)
	@ResponseBody
	public List<Statistics> getEmployeeRecordsData(@PathVariable final int projectId) {
		LOG.info("Getting employee records data.");
		return statisticsService.getEmployeeRecordsByProject(projectId);
	}

}
